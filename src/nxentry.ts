import { tr, td, VNode } from "@cycle/dom";
import _ from "underscore";

export interface NXentry {
	title: string;
	simulation_mode: boolean;
	user_name: string[];
	local_contact: string;
	user_institute: string;
	experiment_identifier: string;
	instrument_name: string;
	sample_id: string;
	run_number: number;
}

function qp(node: Element, key: string): string {
	return node.getElementsByTagName(key)[0].childNodes[0].nodeValue;
}

export function parseNXentry(node: Element): NXentry {
	let result = {
		title: qp(node, "title"),
		simulation_mode: qp(node, "simulation_mode") === "YES",
		user_name: qp(node, "user_name").split(",").sort(),
		local_contact: qp(node, "local_contact"),
		user_institute: qp(node, "user_institute"),
		experiment_identifier: qp(node, "experiment_identifier"),
		instrument_name: qp(node, "instrument_name"),
		sample_id: qp(node, "sample_id"),
		run_number: parseInt(qp(node, "run_number"), 10),
	}
	return result;
}

export enum Column {
	Run = "Run #",
	Title = "Title",
	Simulation = "Simulation",
	UserName = "User Name",
	LocalContact = "Local Contact",
	Institute = "Institute",
	Experiment = "Experiment",
	Instrument = "instrument",
	Sample = "sample"
};

export function getKeyFromValue(value: string): Column {
    switch (value) {
	case Column.Run: return Column.Run;
	case Column.Title: return Column.Title;
	case Column.Simulation: return Column.Simulation;
	case Column.UserName: return Column.UserName;
	case Column.LocalContact: return Column.LocalContact;
	case Column.Institute: return Column.Institute;
	case Column.Experiment: return Column.Experiment;
	case Column.Instrument: return Column.Instrument;
	case Column.Sample: return Column.Sample;
    }
    return Column.Title;
}

export const columns = [Column.Simulation, Column.UserName, Column.LocalContact, Column.Institute, Column.Experiment, Column.Instrument, Column.Sample];

function accessor(row: NXentry, col: Column) {
	switch (col) {
		case Column.Run: return "" + row.run_number;
		case Column.Title: return row.title;
		case Column.Simulation: return row.simulation_mode ? "YES" : "NO";
		case Column.UserName: return row.user_name.join(", ");
		case Column.LocalContact: return row.local_contact;
		case Column.Institute: return row.user_institute;
		case Column.Experiment: return row.experiment_identifier;
		case Column.Instrument: return row.instrument_name;
		case Column.Sample: return row.sample_id;
	};
}

export function rowNXentry(row: NXentry, cols: Column[]): VNode {
	return tr(_.map(cols, col => td(accessor(row, col))));
}
