import xs, { Stream } from 'xstream';
import { run } from '@cycle/run';
import { makeDOMDriver, div, input, MainDOMSource, option, select, table, tr, th, tbody, ul, li, label } from '@cycle/dom';
import { HTTPSource, makeHTTPDriver } from '@cycle/http';
import { NXentry, parseNXentry, rowNXentry, columns, Column, getKeyFromValue } from "./nxentry";
import _ from 'underscore';

const instruments = ["ALF", "CRISP", "EMU", "ENGINX", "EVS", "GEM", "HIFI", "HRPD", "INES", "INTER", "IRIS", "LET", "LARMOR", "LOQ", "MAPS", "MARI", "MERLIN", "MUSR", "NIMROD", "OFFSPEC", "OSIRIS", "PEARL", "POLARIS", "POLREF", "SANDALS", "SANS2D", "SURF", "SXD", "TOSCA", "VESUVIO", "WISH", "ZOOM"];

//Filter the rows down the the values of interest
function run_filter(DOM: MainDOMSource, runs: Stream<NXentry[]>) {
	let filters = DOM
		.select('#filter')
		.events("input")
		.map(ev => (ev.target as HTMLInputElement).value)
		.startWith("");
	return {
		DOM: xs.of(div([
			label("Filter"),
			input({ attrs: { id: "filter" } })])),
		filtered: xs.combine(filters, runs)
			.map(([key, local]) =>
				_.filter(local, y =>
					y.title.search(key) >= 0))
	};
}

//Allow the user to select their columns of interest
export function column_picker(sources: { DOM: MainDOMSource }) {
	let choices = sources.DOM
		.select("input")
		.events("change")
		.map(ev => {
			return {
				name: getKeyFromValue((ev.target as HTMLInputElement).name),
				value: (ev.target as HTMLInputElement).checked
			}
		})
		.fold((acc: Column[], x) => {
			if (!_.contains(acc, x.name) && x.value) {
				acc.push(x.name);
			}
			if (_.contains(acc, x.name) && !x.value) {
				return acc.filter(y => x.name != y);
			}
			return acc;
		}
			, [])
		.map(x => [Column.Run, Column.Title].concat(x));
	let display = ul(
		_.map(columns, x => li([input({ attrs: { type: "checkbox", name: x } }), label(x)]))
	);
	return { DOM: xs.of(display), columns: choices };
}

// Get chose the instrument and cycle and get the runs off the server.
function cyclePicker(sources: { DOM: MainDOMSource, HTTP: HTTPSource }) {
	let parser = new DOMParser();

	let cycleChoice = sources.DOM
		.select('#cycle')
		.events("input")
		.map(ev => (ev.target as HTMLInputElement).value)
	let instrumentChoice = sources.DOM
		.select('#instruments')
		.events("input")
		.map(ev => (ev.target as HTMLInputElement).value)
		.startWith("LARMOR")
	let cycleBody = sources.HTTP
		.select("cycles")
		.map(stream =>
			stream.map(req =>
				_.map(parser.parseFromString(req.text, "text/xml").getElementsByTagName("file"), node => node.getAttribute("name")))
				.replaceError(() => xs.of([]))
		)
		.flatten()
	let display = cycleBody
		.map(xs => div({},
			[select({ attrs: { id: "instruments" } },
				_.map(instruments, (x: string) => option({ attrs: { value: x } }, x))),
			label("Cycle"),
			select({ attrs: { id: "cycle" } },
				_.map(xs, (x: string) => option({ attrs: { value: x } }, x)))]));
	let cyclesRequest = instrumentChoice.map(instr => ({
		url: "http://127.0.0.1:8080/journals/ndx" + instr.toLowerCase() + "/journal_main.xml",
		category: 'cycles',
		withCredentials: true,
		header: {
			Origin: "https://127.0.0.1:8080",
		},
	}));
	let journalRequest = xs.combine(instrumentChoice, cycleChoice)
		.map(([instr, choice]) => {
			return {
				url: "http://127.0.0.1:8080/journals/ndx" + instr.toLowerCase() + "/" + choice,
				category: 'journal',
				withCredentials: true,
				header: {
					Origin: "https://127.0.0.1:8080",
				},
			}
		});
	let reqbody = sources.HTTP
		.select("journal")
		.map(stream => {
			return stream.map(req => _.map(parser.parseFromString(req.text, "text/xml").getElementsByTagName("NXentry"), parseNXentry))
				.replaceError(() => xs.of([]))
		})
		.flatten()
		.startWith([]);
	return {
		DOM: display,
		HTTP: xs.merge(cyclesRequest, journalRequest),
		runs: reqbody
	};
}

function main(sources: { DOM: MainDOMSource, HTTP: HTTPSource }) {
	let dataSource = cyclePicker(sources);
	let temp = run_filter(sources.DOM, dataSource.runs);
	let picker = column_picker(sources);
	let reqOut = xs.combine(temp.filtered, picker.columns)
		.map(([req, cols]) =>
			div({ attrs: { id: "mainTable" } }, [
				table([
					tr(_.map(cols, (x: string) => th(x))),
					tbody(_.map(req, row => rowNXentry(row, cols)))]
				)]));
	let result = xs.combine(dataSource.DOM, picker.DOM, temp.DOM, reqOut).map(
		([a, b, c, d]) => div({ attrs: { id: "inner" } }, [
			div({ attrs: { id: "sideBar" } }, [a, b, c]), d]))
	const sinks = {
		HTTP: dataSource.HTTP,
		DOM: result
	};
	return sinks;
}

const drivers = {
	DOM: makeDOMDriver('#app'),
	HTTP: makeHTTPDriver()
};

run(main, drivers);
