# Junior-Varsity

This is a partial re-implementation of the ISIS journal viewer in
typescript.  I know that the name is terrible, but I have been
completely unable to think of a new name after discovering that the
original repo was named "jv".

## Usage

At the moment, to run this viewer, you'll need shell access and `npm`
installed.

Go to the project directory and run `npm install`.  This will install
all of the required software.  The run `npm run server` to start a
local webserver.  Then go to http://127.0.0.1:8080/ in your browser to
access the journal viewer.

While the code should be self-contained enough to run off of a static
site, the local server needs to run as a webserver because the ISIS
data server breaks the CORS spec in two ways.  First, CORS requires
that the HTTP `OPTIONS` request cannot require authorisation (since
this could possible leak credentials).  The ISIS server demands this
authorisation, against spec.  Once the `OPTIONS` pre-flight has been
performed, the final result will return an
`Access-Control-Allow-Origin` header, specifying which pages are
allowed to access this data.  The ISIS data server returns a wildcard
origin, allowing all servers.  However, the spec states that servers
which require authorisation cannot use a wildcard origin, since
authorization implies a higher level of security.
